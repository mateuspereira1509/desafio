import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DesafioTeste {

    private WebDriver driver;

    @Before
    public void abrir(){
        System.setProperty("webdriver.gecko.driver","C:/Users/Mateus/Desktop/driver/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
    }
    @After
    public void sair(){driver.quit();}
    @Test
    public void testeSubmit() throws InterruptedException {
        Thread.sleep(3000);
        //Username
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[1]/td/input")).sendKeys("Mateus");
        Thread.sleep(3000);
        //Password
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[2]/td/input")).sendKeys("123456789");
        Thread.sleep(3000);
       //TextArea Comment
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[3]/td/textarea")).sendKeys("teste");
        Thread.sleep(3000);
        //Checkbox items
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[2]")).click();
        //clicando no checkbox3 para permanecer apenas o checkbox2 selecionado
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[3]")).click();
        Thread.sleep(3000);
        //radio
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[6]/td/input[2]")).click();
        Thread.sleep(3000);
        //Multiple Select Values
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[2]")).click();
        Thread.sleep(3000);
        //Clicando no Selection item 4 para permanecer apenas o 2 selecionado
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[4]")).click();
        Thread.sleep(3000);
        //clicando no submit
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[9]/td/input[2]")).click();
        Thread.sleep(3000);
        //validando
        Assert.assertEquals("Processed Form Details",driver.findElement(By.xpath("/html/body/div/h1")).getText());
        Thread.sleep(3000);
        //Go back to the form
        driver.findElement(By.id("back_to_form")).click();
        Thread.sleep(3000);

    }
    @Test
    public void testeCancel() throws InterruptedException {
        Thread.sleep(3000);
        //Username
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[1]/td/input")).sendKeys("Mateus");
        Thread.sleep(3000);
        //Password
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[2]/td/input")).sendKeys("123456789");
        Thread.sleep(3000);
        //TextArea Comment
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[3]/td/textarea")).sendKeys("teste");
        Thread.sleep(3000);
        //Checkbox items
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[2]")).click();
        //clicando no checkbox3 para permanecer apenas o checkbox2 selecionado
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[3]")).click();
        Thread.sleep(3000);
        //Radio
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[6]/td/input[2]")).click();
        Thread.sleep(3000);
        //Multiple Select Values
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[2]")).click();
        Thread.sleep(3000);
        //Clicando no Selection item 4 para permanecer apenas o 2 selecionado
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[4]")).click();
        Thread.sleep(3000);
        //Clicando no Cancel
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[9]/td/input[1]")).click();
        Thread.sleep(3000);
        //Validando
        Assert.assertEquals("Comments...",driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[3]/td/textarea")).getText());
        Thread.sleep(3000);
    }





}
